#!/bin/bash - 
#===============================================================================
#
#          FILE: bitwise_negation.sh
# 
#         USAGE: ./bitwise_negation.sh
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/25/2015 16:53
#      REVISION:  ---
#===============================================================================

#===============================================================================
set -o nounset                              # Treat unset variables as an error

declare -i answer
declare -i number1=10
declare -i negative_number=-10
declare -ir ONE=1
declare -ir ZERO=0

echo -e "\nBitwise negation"
echo -e "Starting number1: $number1"
printf "%x\n" $number1
answer=$(( ~number1 ))
echo -e "Answer: $answer"
echo "obase=2; $answer" | bc
echo -e "number1: $number1"
echo "obase=2; $number1" | bc

echo -e "\nBitwise negation"
echo -e "Starting negative_number: $negative_number"
answer=$(( ~negative_number))
echo -e "Answer: $answer"
echo -e "Negative Number: $negative_number"

echo -e "\nBitwise negation"
echo -e "Starting ONE: $ONE"
answer=$(( ~ONE))
echo -e "Answer: $answer"
echo -e "ONE: $ONE"

echo -e "\nBitwise negation"
echo -e "Starting ZERO: $ZERO"
answer=$(( ~ZERO ))
echo -e "Answer: $answer"
echo -e "Number: $ZERO"

