#!/bin/bash - 
#===============================================================================
#
#          FILE: uniary_plusminus.sh
# 
#         USAGE: ./uniary_plusminus.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/26/2015 17:23
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare -i answer
declare -i number1=10
declare -i negative_number=-10
declare -ir ONE=1
declare -ir ZERO=0

echo -e "\nUniary Plus"
echo -e "Starting negative number: $negative_number"
answer=$(( +negative_number ))
echo -e "Answer: $answer"
echo -e "Negative Number: $negative_number"

echo -e "\nUniary Minus"
echo -e "Starting number1: $number1"
answer=$(( -number1 ))
echo -e "Answer: $answer"
echo -e "Number: $number1"


