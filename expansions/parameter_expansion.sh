#!/bin/bash - 
#===============================================================================
#
#          FILE: parameter_expansion.sh
# 
#         USAGE: ./parameter_expansion.sh 
# 
#   DESCRIPTION: This script is a demonstration of Bash parameter / variable 
#                expansion. It purposely excludes array expansions. 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: See Bash Reference Manual 3.5.3 Shell Parameter Expansion.
#
#                Bash refers to the standard definition of variable using the 
#                term parameter.
#                It doesn't differentiate between the standard use of paramter,
#                meaning an argument passed to a function or  procedure, and 
#                
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/25/2015 11:26
#      REVISION:  ---
#===============================================================================

# This is turned off to demotrate expansions of unset variables
# set -o nounset                              # Treat unset variables as an error

declare assigned_var="assigned"
declare var_w_numbers="Flip4Real"
declare var2="var2_value"
declare upper_case_var="THIS WAS ALL UPPER CASE"
declare lower_case_var="this was all lower case"
declare unassigned_var
declare var_who_s_value_is_the_name_of_another_var="other_var"
declare other_var="other_var value"


echo -e "Basic Parameter Expansion Form"
# General Form
# $parameter
# The value of parameter is substituted.
echo -e $assigned_var

echo -e "\nSimple Parameter Expansion"
# General Form
#   ${parameter }
# The value of parameter is substituted.
echo -e ${assigned_var}

echo -e "\nIndirect Parameter Expansion"
# General Form
#   ${!parameter }
# The value of parameter is substituted, then this value is used as the variable
# name whose value forms the final substitution
echo -e ${!var_who_s_value_is_the_name_of_another_var}

echo -e "\nParameter Expansion with substitution"
# General Form:
#   ${parameter:−word}
# If parameter is unset or null, the expansion of word is substituted. Otherwise,
# the value of parameter is substituted
echo -e ${assigned_var:-substitute}
echo -e ${unassigned_var:-substitute}
echo -e ${unassigned_var} 

echo -e "\nParameter Expansion with substitution and assignment"
# General Form:
#   ${parameter:=word}
# If parameter is unset or null, the expansion of word is assigned to parameter.
# The value of parameter is then substituted. 
echo -e ${unassigned_var:=substitute}
echo -e ${unassigned_var}

echo -e "\nParameter Expansion without substitution"
# General Form:
#   ${parameter:+word}
# If parameter is unset or null, nothing is substituted
# otherwise the expansion of word is substituted. 
unassigned_var="" #Set back to null
echo -e ${assigned_var:+Not Empty}
echo -e ${unassigned_var:+Empty}

echo -e "\nSubstring Expansion"
# General Form:
#   ${parameter:offset}
#   ${parameter:offset:length}
# expands to up to length characters of the value of parameter starting at the
# character specified by offset. Offset starts at zero.
#
# if length is omitted, it expands to the substring of the value of parameter 
# starting at the character specified by offset and extending to the end of the value.
# This character must be a number
#
# If offset evaluates to a number less than zero, the value is used as an offset
# in characters from the end of the value of parameter. 
#
# If length evaluates to a number less than zero, it is interpreted as an offset 
# in characters from the end of the value of parameter rather than a number of 
# characters, and the expansion  is the characters between offset and that result
#
# Note that a negative offset must be separated from the colon by at least one space
# to avoid being confused with the ‘:-’ expansion.

echo -e ${var_w_numbers:4}
echo -e ${assigned_var:3:4}
echo -e ${assigned_var: -6}
echo -e ${assigned_var: -5:2}
echo -e ${assigned_var: -5:-2}

echo -e "\nParameter Name Expansion"
# General Form:
#   ${!prefix*}
#   ${!prefix@}
# Here the prefix is used to search for all variables that start with the same
# characters.  The expressions then expands to a list of all the matches
echo -e ${!var*}
echo -e ${!var@}

echo -e "\nParameter Length Expansion"
# General Form:
#   ${#parameter}
# This expands to the length of the parameter value as an integer
echo -e ${#var_w_numbers}

echo -e "\nParameter Pattern Prefix Removal Expansion"
# General Form:
#   ${parameter#word}
# Here the value of the parameter is searched based on the word pattern and 
# any matching leading characters are removed.
echo -e ${var_w_numbers#Flip}

echo -e "\nParameter Pattern Suffix Removal Expansion"
# General Form:
#   ${parameter#word}
# Here the value of the parameter is searched based on the word pattern and 
# any matching trailing characters are removed.
echo -e ${var_w_numbers%Real}

echo -e "\nParameter Pattern Replacement Expansion"
# General Form:
#   ${parameter/search/replace}
# Here the value of the parameter is searched for the search pattern and 
# any matches are replaced with the replacement pattern
echo -e ${var_w_numbers/Real/Fake}

echo -e "\nParameter Pattern Case Conversion"
# General Form:
#   ${parameter^search}
#   ${parameter^^search}
#   ${parameter,search}
#   ${parameter,,search}
# Here the value of the parameter is searched for the search pattern and 
# the first match is changed to upper case (^), or lower case (,)
# Or in the casse of the double operators (^^ ,,) all of the matching characters
# are changed.
echo ${lower_case_var^[[:alpha:]]}
echo ${lower_case_var^^[[:alpha:]]}
echo ${upper_case_var,[[:alpha:]]}
echo ${upper_case_var,,[[:alpha:]]}

echo -e "\nParameter Expansion with substitution and writing to standard error"
# General Form:
#   {parameter:?word}
# If parameter is unset or null, the expansion of word is written to standard 
# error and the shell otherwise the value of parameter is substituted. 
# Note this error condition halts script execution and returns a status code of 1
unassigned_var="" #Set back to null
echo -e ${assigned_var:?Error Message}
echo -e ${unassigned_var:?Error Message}

