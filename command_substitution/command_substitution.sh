#!/usr/bin/env bash

#===============================================================================
# 
#         USAGE: ./command_substitution.sh 
# 
#   DESCRIPTION: Demonstration of command substitution
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: See Bash Reference Manual
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare current_user
declare -i uptime_days

echo -e "\nCommand Substitution Bracket Form"
# General Form:
#   $(command) 
# Replaces the command substitution expression with the standard output of the
# command, while removing any trailing newlines.  
# This stores the current user in a variable
current_user=$(whoami)
echo "${current_user}"

echo -e "\nCommand Substitution Backtick Form"
# General Form:
#   `command` 
# Replaces the command substitution expression with the standard output of the
# command, while removing any trailing newlines
# This shows the number of days your system has been up
uptime_days=`uptime | cut -d " " -f4`
echo "${uptime_days}"

echo -e "\nCommand Substitution Nested"
#Note that quotes are used to keep newlines
echo -e "$(lastlog -u $(whoami))"