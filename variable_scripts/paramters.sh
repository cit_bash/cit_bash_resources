#!/bin/bash - 
#===============================================================================
#
#          FILE: paramters.sh
# 
#         USAGE: ./paramters.sh 
# 
#   DESCRIPTION: demonstrate the bash special parameters
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: 
#             For a full description of parameters see The Bash Reference CH 3.4
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/24/2015 14:28
#      REVISION:  ---
#===============================================================================
set -o nounset                              # Treat unset variables as an error
declare -ri NUMARGS=$#

# See info coreutils 'printf invocation' 
# and http://wiki.bash-hackers.org/commands/builtin/printf
# for further printf information
printf "\n%s\n" "Script Aruguments" 

# `$@` expands to the all positional parameters
printf "%s  " "\$@: $@"

# `$0` Expands to the name of the shell or shell script. 
printf "\n%s\n" "\$0: $0"

# `$#` Expands to the number of positional parameters in decimal.
printf "%s\n" "\$#: $#"
printf "%s\n" "\${#@}: ${#@}"
printf "%s\n" "\${#*}: ${#*}"

# `$?` Expands to the exit status of the most recently executed foreground pipeline.
printf "%s\n" "\$?: $?"

# `$-` Expands to the current option flags as specified upon invocation
printf "%s\n" "\$-: $-"

# `$$` Expands to the process id of the shell. 
printf "%s\n" "\$$: $$"

# `$_` expands to the last argument to the previous command 
printf "%s\n" "\$_: $_"

#Loop through all positional arguments and print them to standard output
for ((i=1 ; i <= NUMARGS ; i++))
do
    # Parameters `$1` to `$999...` contains individual positional parameters. 
    printf "%s\n" "\$$i: $1"
    shift #Shift moves the postional parameters down one and discards the parameter in position 1

done



