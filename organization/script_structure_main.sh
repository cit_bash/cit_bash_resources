#!/usr/bin/env bash

# Demonstrates recommended structure for bash scripts and libraries
# works in conjunction with script_structure_sub.sh

set nounset

# Get the directory of this script
declare -r script_dir="$(dirname "${BASH_SOURCE[0]}")" #can't symlink this script

#Set a global literal to document where the script started
declare starting_point="started in script_structure_main.sh"


#######################################
# Organizing function that outputs its location and callstack then calls 
# functions in current script file
# Globals:
#   script_dir
#   starting_point
# Arguments:
#   None
# Outputs:
#   Starting script global
#   File Stack (BASH_SOURCE)
#   Function Stack(FUNCNAME)
#######################################
mainscript_main () {
    echo -e "\n********** mainscript_main function**********"
    echo "starting_point: ${starting_point}"
    echo "File Stack (BASH_SOURCE): ${BASH_SOURCE[*]}"
    echo "Function Stack(FUNCNAME): ${FUNCNAME[*]}"

    mainscript_func1
    source "$script_dir/script_structure_sub.sh"
    subscript_func1
}

#######################################
# Function that outputs its location and callstack
# Globals:
#   script_dir
#   starting_point
# Arguments:
#   None
# Outputs:
#   Starting script global
#   File Stack (BASH_SOURCE)
#   Function Stack(FUNCNAME)
#######################################
mainscript_func1() {

    echo -e "\n********** mainscript_func **********"
    echo "starting_point: ${starting_point}"
    echo "File Stack (BASH_SOURCE): ${BASH_SOURCE[*]}"
    echo "Function Stack(FUNCNAME): ${FUNCNAME[*]}"

}

# Only run `main` if this script is being **run**, NOT sourced (imported)
# $0 is the name of the script being run by the bash interpreter - the first argument passed to the interpreter
# ${BASH_SOURCE[0]} is the name of the script being run - even if it is sourced from another script.
if [ "${BASH_SOURCE[0]}" = "$0" ]; then
    echo -e "\n********** script_structure_main.sh outside of function**********"
    echo "starting_point: ${starting_point}"
    echo "File Stack (BASH_SOURCE): ${BASH_SOURCE[*]}"
    echo "Function Stack(FUNCNAME): ${FUNCNAME[*]}"

    mainscript_main "$@"
fi