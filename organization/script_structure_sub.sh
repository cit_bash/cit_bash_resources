#!/usr/bin/env bash
#
# Demonstrates recommended structure for bash scripts and libraries
# works in conjuction with script_structure_main.sh

set nounset

#######################################
# Orgainizing function that outputs its location and callstack and calls functions
# in current and a sourced script files
# Globals:
#   script_dir
#   starting_point
# Arguments:
#   None
# Outputs:
#   Starting script global
#   File Stack (BASH_SOURCE)
#   Function Stack(FUNCNAME)
#######################################
subscript_main() {
  # Create starting point if it hasn't been globally set
  echo -e "\n********** subscript_main **********"
  echo "starting_point: ${starting_point}"
  echo "File Stack(BASH_SOURCE): ${BASH_SOURCE[*]}"
  echo "Function Stack(FUNCNAME): ${FUNCNAME[*]}"

  subscript_func1
}

#######################################
# Function that outputs its location and function callstack
# Globals:
#   script_dir
#   starting_point
# Arguments:
#   None
# Outputs:
#   Starting script global
#   File Stack (BASH_SOURCE)
#   Function Stack(FUNCNAME)
#######################################
subscript_func1() {
  echo -e "\n********** subscript_func1 **********"
  echo "starting_point: ${starting_point}"
  echo "File Stack(BASH_SOURCE): ${BASH_SOURCE[*]}"
  echo "Function Stack(FUNCNAME): ${FUNCNAME[*]}"

}

# Only run `main` if this script is being **run**, NOT sourced (imported)
# $0 is the name of the script being run by the bash interpreter - the first argument passed to the interpreter
# ${BASH_SOURCE[0]} is the name of the script being run - even if it is sourced from another script.
if [ "${BASH_SOURCE[0]}" = "$0" ]; then
    declare starting_point="started in script_structure_sub.sh"

    echo -e "\n********** script_structure_sub.sh outside of function**********"
    echo "starting_point: ${starting_point}"
    echo "File Stack (BASH_SOURCE): ${BASH_SOURCE[*]}"
    echo "Function Stack (FUNCNAME): ${FUNCNAME[*]}"

  subscript_main "$@"
fi
