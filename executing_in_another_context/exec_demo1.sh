#!/usr/bin/env bash
#
#
#

set -o nounset                              # Treat unset variables as an error

# Following sets the variable to the current script file name removing the 
#   leading path, specifically
# ${0} captures scriptname using parameter expansion
# ## invokes prefix removal 
# */ is the pattern to remove i.e all characters up to the last /
#   i.e. everything but the relative path name
declare -rx SCRIPT=${0##*/} # the variable is exported (-x) and readonly (-r) 


#declare a readonly variable for the script to be executed
declare -r script_to_exec="./exec_demo_script2.sh"

#Verify the that script2 is executable (-x) if not (!) 
if [[ ! -x script_to_exec ]] ; then
  printf "%s" "${SCRIPT}:${LINENO}: the script ${script_to_exec} is not executable" >&2
  exit 192
fi

printf "$SCRIPT: transferring control to $script_to_exec, never to return...\n"
exec "${script_to_exec}"

# If the following line has been reached the exec statement didn't call the other
# script
printf "$SCRIPT:$LINENO: exec failed!\n" >&2

exit 1 #Make sure the caller knows the exec failed
