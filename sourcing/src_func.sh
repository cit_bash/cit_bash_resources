#!/bin/bash - 
#===============================================================================
#
#          FILE: src_func.sh
# 
#         USAGE: ./src_func.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/24/2015 19:09
#      REVISION:  ---
#===============================================================================

function print_stuff {
  declare frmt=$1
  declare script_name=$2
  printf $frmt "Executing: $script_name"
  return 0
}

