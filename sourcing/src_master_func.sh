#!/bin/bash - 
#===============================================================================
#
#          FILE: src_master_func.sh
# 
#         USAGE: ./src_master_func.sh 
# 
#   DESCRIPTION: This demonstates the ability of one script to include others
#                and introduces functions
# 
#       OPTIONS: ---
#  REQUIREMENTS: src_sub1_func.sh, src_sub2_func.sh, and src_func.sh
#                directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/24/2015 13:31
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare script_name=$0

. ./src_func.sh
print_stuff "%b\n\n" $script_name
source ./src_sub1_func.sh
. ./src_sub2_func.sh

print_stuff "\n%b" $script_name
