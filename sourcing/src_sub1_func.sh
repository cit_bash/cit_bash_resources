#!/bin/bash - 
#===============================================================================
#
#          FILE: src_sub1_func.sh
# 
#         USAGE: ./src_sub1_func.sh
# 
#   DESCRIPTION: This is part of the sourcing demonstration
# 
#       OPTIONS: ---
#  REQUIREMENTS: src_func
#          BUGS: ---
#         NOTES: This script is intended to be invoked from source_master.sh
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/24/2015 13:32
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

print_stuff "\t%s\n" "src_sub1_func.sh"
