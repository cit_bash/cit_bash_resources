#!/bin/bash - 
#===============================================================================
#
#          FILE: scoping.sh
# 
#         USAGE: ./scoping.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/27/2015 11:18
#      REVISION:  ---
#===============================================================================

#set -o nounset                              # Treat unset variables as an error

function declare_vars {
  declare func_declared="Function Declared Variable"
  func_not_declared="Function Variable Not Declared"
  declare -x func_declared_export="Function Declared Variable with Export Attribute"
  export FUNC_EXPORT="Function Exported Variable"

}

display_vars () {
  echo -e "\n**** Display Vars Function Output ****"
  echo -e "ms_declared: \t\t$ms_declared"
  echo -e "ms_not_declared: \t$ms_not_declared"
  echo -e "ms_declared_export: \t$ms_declared_export"
  echo -e "MS_EXPORT: \t\t$MS_EXPORT"
  echo -e "func_declared: \t\t$func_declared"
  echo -e "func_not_declared: \t$func_not_declared"
  echo -e "func_declared_export: \t\t$func_declared_export"
  echo -e "FUNC_EXPORT: \t\t$FUNC_EXPORT"
  echo -e "**** End Display Vars Function Output ****\n"
}

declare_vars

declare ms_declared="Main Script Declared Variable"
ms_not_declared="Main Script Variable Not Declared"
declare -x ms_declared_export="Main Script Declared Variable With Export Attribute"
export MS_EXPORT="Main Script Exported Variable"

echo -e "\n**** Display Vars Inline Output****"
echo -e "ms_declared: \t\t$ms_declared"
echo -e "ms_not_declared: \t$ms_not_declared"
echo -e "ms_declared_export: \t$ms_declared_export"
echo -e "MS_EXPORT: \t\t$MS_EXPORT"
echo -e "func_declared: \t\t$func_declared"
echo -e "func_not_declared: \t$func_not_declared"
echo -e "func_declared_export: \t$func_declared_export"
echo -e "FUNC_EXPORT: \t\t$FUNC_EXPORT"
echo -e "**** End Display Vars Inline Output ****\n"

bash ./called_script.sh
source ./sourced_script.sh
display_vars
