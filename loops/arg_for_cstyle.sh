#!/bin/bash - 
#===============================================================================
#
#          FILE: arg_loop.sh
# 
#         USAGE: ./arg_loop.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Loops through script arguments using a for loop
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/29/2015 12:12
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

echo 'a'
echo 'b'

# Loop through all of the arguments starting at 1 (i=1) up until the total number of
# arguments (i <= $#), increasing the argument postition by one each time (i++)
for ((i=1; i <= $#; i++)); do

  #Below uses indirection to get the value of the i variable
  # i.e. i is expanded to its value.  This value is treated as a variable name 
  # this variable name is then expanded.
  echo ${!i}

done

