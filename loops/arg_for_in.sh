#!/bin/bash - 
#===============================================================================
#
#          FILE: arg_loop.sh
# 
#         USAGE: ./arg_loop.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Demonstrates looping through all arguments using a for loop
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/29/2015 12:12
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

array=( "this" "is" "an" "example")

echo "${array[2]}"
echo "${array[@]}"

#Loop through all argument (#@ expands to a space seperated list of all arguments)
for arg in "${array[@]}"; do

  #arg is a variable that takes on the value of each of the parameters passed to
  #the script one after the other
  echo -e "$arg\n"
done

