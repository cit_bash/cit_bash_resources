#!/bin/bash - 
#===============================================================================
#
#          FILE: until_numeric.sh
# 
#         USAGE: ./until_numeric.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/03/2015 19:18
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare -i num_times
declare -i counter=1
read -p "Enter the number of times the loop should run:" num_times 

#Note the two variables below don't need to be expanded because of the (()) 
#expression
until ((counter > num_times)) ; do
  printf "\n%s" "Interation: $counter"
  ((counter++))
done

