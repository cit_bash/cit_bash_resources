#!/bin/bash - 
#===============================================================================
#
#          FILE: sed_d.sh
# 
#         USAGE: ./sed_d.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/08/2015 16:16
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
# Delete Lines from the output

# Delete all lines containing 1,2,6
sed '/[126]/d' template.txt

# Delete all lines between one containing _6 and one with a #
sed '/_6/,/#/d' template.txt
