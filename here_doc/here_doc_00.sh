#!/usr/bin/env bash
#
# Simple Here Doc example

set -o nounset                              # Treat unset variables as an error

function say_greeting() {
  #local Variables
  declare user_name
  declare special

  read -p "Enter your name: " user_name
  read -p "What is special about today? " special

  declare today=$(date)

  echo "Hello ${user_name}, today is ${today} and ${special}"
}

#Here Document
cat << TEXTDELIMITER
All of the text between the arbitrary delimeters 
forms the content of the "here document"
TEXTDELIMITER

#Simple Here String
here_string_content="This content will form the body of the here string"
cat <<< $here_string_content

#Capturing output into a varialbe
your_name=$(whoami)
cat <<< $your_name


say_greeting << EOF
John Doe
It's my birthday!
EOF

