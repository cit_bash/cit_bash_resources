#!/usr/bin/env bash
#
# This script demonstrates the following:
#   - here documents
#   - command substitution
#   - variable expansion
#   - BASH_SOURCE varialbe
#   - dirname command 

set -o nounset # Treat undeclared variables as an error, catches typos

# Comman Substituion Examples
# see https://www.gnu.org/software/bash/manual/html_node/Command-Substitution.html
user_name=$(whoami) # Get current user and store in variable 
today=$(date) #Get current date and store in variable

# The following determines the current location of this script and gets its parent
# directory
#
# BASH_SOURCE is an array variable whose members are the source filenames loaded
# by the shell, see 
#   - https://www.gnu.org/software/bash/manual/html_node/Bash-Variables.html
#   - https://stackoverflow.com/questions/35006457/choosing-between-0-and-bash-source 
current_script_path="${BASH_SOURCE[0]}"

# dirname strips the filename from a path leaving a directory path
curent_script_dir="$(dirname "${current_script_path}")"


# Here Document Usage
# see https://www.gnu.org/software/bash/manual/html_node/Redirections.html
#
# This bash command creates a file named demo.cfg in the same directory as the
# current script, and writes some information to it. 
# Here is a breakdown of each line and variable:
# 
# - cat is used to write the output to a demo.cfg file
# - << __delim__ is a syntax that tells the shell to read the input until it 
#     encounters a line containing __delim__. This is the here document or a heredoc
# - > is a redirection operator that sends the output of the command to a file
# - "${curent_script_dir}/demo.cfg" this is the path of the file where the output
#     will be written, generated using the variable ${curent_script_dir} 
#     It is enclosed in curly braces to prevent word splitting during expansion
# - The text between the end of the invocation of cat and the __delim__ with 
#     written to the file. It includes the variables user_name and today which
#     are expanded by the shell.
# - __delim__ is the end marker of the here document. It tells the shell to stop
#     reading the input and write it to the file.

cat << __DELIM__ > "${curent_script_dir}/demo.cfg"
[info]
  user="${user_name}"
  date="${today}"
__DELIM__


# The following is an alternative for the here document. 
# It uses the same syntax as the here document but the end marker is quoted
# This prevents the shell from expanding variables in the here document
# Also not the delimiter can be any string, it doesn't have to be __DELIM__
cat << "EOF" >> "${curent_script_dir}/demo.cfg"
[part 2]
  user="${user_name}"
  date="${today}"
EOF

