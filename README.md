# Bash resources for CIT students


## External Resources

1. [Bash Reference Manual](https://www.gnu.org/software/bash/manual/html_node/index.html)
1. [Bash Hackers Wiki](https://flokoe.github.io/bash-hackers-wiki/)
1. [Google Bash Style Guide](https://google.github.io/styleguide/shellguide.html)
1. [The Linux Command Line](https://learning.oreilly.com/library/view/the-linux-command/9781492071235/)
1. [Bash Cookbook](https://learning.oreilly.com/library/view/bash-cookbook-2nd/9781491975329/)
1. [Mastering Bash Shell Scripting: Video Course](https://learning.oreilly.com/videos/mastering-bash-shell/9781801070607/)
1. [Bash Scripting Cheat-sheet](https://devhints.io/bash)
1. [Bash Idioms (Stylish Bash)](https://learning.oreilly.com/library/view/bash-idioms/9781492094746/)
