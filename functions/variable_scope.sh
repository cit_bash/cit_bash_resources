#!/usr/bin/env bash
#
# Demonstrates function arguments, local and global variables in BASH
set nounset
declare p1="global_p1"

#######################################
# Main function that calls sub functions and sets variables
# Globals:
#   p1
#   sets p2
# Arguments:
#   None
# Outputs:
#   None
#######################################
main() {
  declare -g p2="global_p2"
  echo "p1: ${p1} p2: ${p2}"
  fun1
  fun2 "literal_arg_p1" "literal_arg_p2"
}

#######################################
# Sub function that uses global variables
# Globals:
#   p1
#   p2
# Arguments:
#   None
# Outputs:
#   Values of p1 and p2
#######################################
fun1() {

  echo "p1: ${p1} p2: ${p2}"
}

#######################################
# Sub function that overloads global variables
# Globals:
#   p1
#   p2
# Arguments:
#   p1
#   p2
# Outputs:
#   Values of loca p1 and p2
#######################################
fun2() {
  declare p1=$1
  declare p2=$2

  echo "p1: ${p1} p2: ${p2}"
}

# Only run `main` if this script is being **run**, NOT sourced (imported)
# $0 is the name of the script being run by the bash interpreter - the first argument passed to the interpreter
# ${BASH_SOURCE[0]} is the name of the script being run - even if it is sourced from another script.
if [ "${BASH_SOURCE[0]}" = "$0" ]; then
  main "$@"
fi

# Demonstrates that the values of the global variables are not changed by the sub functions
echo "p1: ${p1} p2: ${p2}"