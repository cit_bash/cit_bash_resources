#!/usr/bin/env bash
#
# Simple function examples 

set -o nounset                              # Treat unset variables as an error

#Function Declarions
function func1 {
  echo "Doing stuff in func1, here is arg1: ${1}"
  return 0
}

func2() {
  echo "Doing stuff in func2, here is arg2: ${2}"
  echo "First Arg: ${1}"
  return 0
}

echo "Before function invocation"

# Function Invocations
for ((i=1; i<=5; i++)); do
  func1 "${i}"
done

func2 stuff "This will be the second argument for func2"













